#!/usr/bin/env python3
import sys

def usage(progname):
    print("Usage: {0} <ipv4/mask>\nExample: {0} 192.168.1.1/24".format(progname))
    sys.exit(1)

def start_ip(interface_ip, slash):
    result = interface_ip.split(".")
    last = int(result[3])
    
    if last < 254:
        last += 1

    return "{0}.{1}.{2}.{3}".format(result[0], result[1], result[2], last)

# Given an IP address and a mask, compute the last usable IP of the range
def end_ip(interface_ip, slash):
    result = [None,None,None,None]
    ipv4 = interface_ip.split(".")
    netmask=(2**32-1) - (2**(32-slash)-1)

    for i in range(0,4):
        block_ip = int(ipv4[3 - i])
        block_netmask = (netmask & (0xff<<i*8))>>i*8
        block_set_ip =  (~block_netmask & 0xff)
        result[i] = (block_ip & block_netmask) | block_set_ip

    # Don't use broadcast addresses
    if (result[3] == 255):
        result = 254

    # Now return a human readable result
    return "{0}.{1}.{2}.{3}".format(result[3], result[2], result[1], result[0])

# Sanity check: TODO: regex to check
#print(mask(int(sys.argv[1])))

interface_ip = sys.argv[1].split("/")[0]
mask_slash_notation=int(sys.argv[1].split("/")[1])
print("{start_ip},{end_ip},12h".format(
    start_ip=start_ip(interface_ip, mask_slash_notation),
    end_ip=end_ip(interface_ip, mask_slash_notation)
))
