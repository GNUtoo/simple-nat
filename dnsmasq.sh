#!/bin/sh

usage()
{
  echo "$0 <interface> [interface [interface [...]]]"
  exit 1
}

find_iface_ip_and_range()
{
  interface="$1"
  ipv4_with_mask="$(ip addr show dev ${interface} | grep inet | awk '{print $2}')"
  ipv4="$(echo ${ipv4_with_mask} | awk -F '/' '{print $1}')"
  dhcp_range="$($(dirname $0)/ip_range.py ${ipv4_with_mask})"
  echo ${ipv4}
  echo ${dhcp_range}
}

if [ $# -lt 1 ] ; then
  usage
fi

thisdir=$(dirname $0)
ifaces_configs=""
extra_config_file="$(dirname $0)/dnsmasq.conf.local"
if [ ! -f "${extra_config_file}" ] ; then
    extra_config_file=""
else
    extra_config_file="--conf-file=${extra_config_file}"
fi

for iface in $@ ; do
  ip link set dev ${iface} up
  iface_ipv4="$(find_iface_ip_and_range ${iface} | head -n1)"
  iface_dhcp_range="$(find_iface_ip_and_range ${iface} | tail -n1)"
  ifaces_configs="${ifaces_configs} --interface=${iface} "
  ifaces_configs="${ifaces_configs} --dhcp-range=set:${iface},${iface_dhcp_range} "
  ifaces_configs="${ifaces_configs} --dhcp-option=tag:${iface},121,${iface_ipv4} "
done

dnsmasq \
  --no-daemon \
  --except-interface=lo \
  --bind-interfaces \
  --dhcp-script=/bin/echo \
  ${ifaces_configs} \
  "${extra_config_file}"
