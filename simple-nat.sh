#!/bin/sh
set -e

usage()
{
  echo "Usage: $0 <internet_interface> <sharing_interface [sharing interface [...]]>"
  exit 1
}

if [ $# -lt 2 ] ; then
  usage
fi

interface="$1"
shift 1

for sharing_interface in $@ ; do
  $(dirname $0)/nat.sh start "${interface}" "${sharing_interface}"
done

$(dirname $0)/dnsmasq.sh "${sharing_interface}"
