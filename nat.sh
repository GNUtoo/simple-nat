#!/bin/sh

usage()
{
  echo "Usage:"
  echo "$0 start <internet_interface>"
  echo "$0 start <internet_interface> <sharing_interface>"
  echo "$0  stop <internet_interface>"
  echo "$0  stop <internet_interface> <sharing_interface>"
  exit 1
}

if [ $# -ne 2 -a $# -ne 3 ] ; then
  usage
elif [ "$1" != "start" -a "$1" != "stop" ] ; then
  usage
fi

if_internet="-o $2"
if_sharing=""
addremove=""

if [ $# -eq 3 ] ; then
  if_sharing="-i $1"
fi

if [ "$1" = "start" ] ; then
  echo 1 > /proc/sys/net/ipv4/ip_forward
  addremove="-A"
else
  echo 0 > /proc/sys/net/ipv4/ip_forward
  addremove="-D"
fi

iptables -t nat "${addremove}" POSTROUTING "${if_internet}" -j MASQUERADE

if [ $# -eq 2 ] ; then
  iptables "${addremove}" FORWARD "${if_sharing}" "${if_internet}" -j ACCEPT
else
  iptables "${addremove}" FORWARD "${if_internet}" -j ACCEPT
fi

